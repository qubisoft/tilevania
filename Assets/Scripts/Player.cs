﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    [SerializeField] float playerSpeed = 5f;
    [SerializeField] float jumpSpeed = 5f;
    [SerializeField] float climbSpeed = 5f;

    Rigidbody2D myRigdtBody;
    Animator myAnimator;
    CapsuleCollider2D myBodyCollider2D;
    BoxCollider2D myFeet;

    bool isAlive = true;


    // Start is called before the first frame update
    void Start()
    {
        myRigdtBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myBodyCollider2D = GetComponent<CapsuleCollider2D>();
        myFeet = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAlive) { return; };

        Run();
        FlipSprite();
        Jump();
        climbLadder();
        Deatch();



    }
     
    public void Run()
    {
        float controlThrow = Input.GetAxis("Horizontal"); // value from -1 to +1
        Vector2 playerVelocity = new Vector2(controlThrow * playerSpeed, myRigdtBody.velocity.y);
        myRigdtBody.velocity = playerVelocity;

        //Debug.Log(playerVelocity);

        
        if (Mathf.Abs(myRigdtBody.velocity.x) > Mathf.Epsilon)
        {
            myAnimator.SetBool("Running", true);
        }
        else
        {
            myAnimator.SetBool("Running", false);
        }
    }

    private void climbLadder()
    {
        
        if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Climbing"))) {
            myAnimator.SetBool("Climbing", false);
            myRigdtBody.gravityScale = 1;
            return;
        }
        else
        {
            myRigdtBody.gravityScale = 0;
        }

        float controlThrow = Input.GetAxis("Vertical");
        Vector2 climpVelocity = new Vector2(myRigdtBody.velocity.x, controlThrow * climbSpeed);
        myRigdtBody.velocity = climpVelocity;


        bool playerHasVerticalSpeed = Mathf.Abs(myRigdtBody.velocity.y) > Mathf.Epsilon;
        myAnimator.SetBool("Climbing", playerHasVerticalSpeed);
        myRigdtBody.gravityScale = 0;
    }



    private void Jump()
    {
        if (Input.GetButtonDown("Jump"))
        {

            int grountLayer = LayerMask.GetMask("Ground");
            var isOnGround = myFeet.IsTouchingLayers(grountLayer);

            if (isOnGround)
            {
                Vector2 jumpValocityAdd = new Vector2(0f, jumpSpeed);
                myRigdtBody.velocity += jumpValocityAdd;
            }

        }
    }

    private void FlipSprite()
    {        
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigdtBody.velocity.x) > Mathf.Epsilon;
        if (playerHasHorizontalSpeed)
        {
            transform.localScale = new Vector2(Mathf.Sign(myRigdtBody.velocity.x), 1f);
            
        }

    }

    private void Deatch()
    {
        int enemyLayer = LayerMask.GetMask("Enemy");
        int hazardLayer = LayerMask.GetMask("Hazard");
        if (myBodyCollider2D.IsTouchingLayers(enemyLayer) || myBodyCollider2D.IsTouchingLayers(hazardLayer))
        {
            isAlive = false;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
            myAnimator.SetTrigger("Death");

            FindObjectOfType<GameSession>().ProcesPlayerDeath();
        }
    }

}
