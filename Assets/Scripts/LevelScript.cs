﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelScript : MonoBehaviour
{
    [SerializeField] float timeToLoad = 1f;
    [SerializeField] float slowMotionTime = 0.2f;
    int sceneIndex;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        sceneIndex = SceneManager.GetActiveScene().buildIndex;

        StartCoroutine(loadNextScene());
    }

    IEnumerator loadNextScene()
    {
        Time.timeScale = slowMotionTime;
        yield return new WaitForSeconds(timeToLoad);
        Time.timeScale = 1f;
        SceneManager.LoadScene(sceneIndex + 1);
    }
}
